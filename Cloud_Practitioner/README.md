# AWS Certified Cloud Practioner

## Supplementary Learning:

- [AWS sample study questions](https://d1.awsstatic.com/training-and-certification/docs-cloud-practitioner/AWS-Certified-Cloud-Practitioner_Sample-Questions.pdf)
- The official practice exam requires registraion and can be found at [aws.training](https://www.aws.training/certification?src=cert-prep). AWS also provides free online essentials [training](https://aws.amazon.com/training/digital/aws-cloud-practitioner-essentials/) for the CCP exam
- Practice exams purchased from [Udemy](https://www.udemy.com/course/aws-certified-cloud-practitioner-practice-test/)

## Format/Content


Review the [Exam Overview](https://d1.awsstatic.com/training-and-certification/docs-cloud-practitioner/AWS-Certified-Cloud-Practitioner_Exam-Guide.pdf) for expanded guidance.

This exam validates an examinee's ability to:
- Explain the value of the AWS Cloud
- Understand and explain the AWS shared responsibility model
- Understand AWS Cloud security best practices
- Understand AWS Cloud costs, economics, and billing practices
- Describe and position the core AWS services, including compute, network, databases, and storage
- Identify AWS services for common use cases

There will be two types of questions:
- _Multiple choice_: one correct response + three incorrect distractors
- _Multiple response_: two or more correct responses out of five or more options

No penalty for guessing. Unanswered questions are scored as incorrect.

_Exam is PASS or FAIL_, scored from 100-1000 with a minimum massiong score of 700. The exam uses a compensatory scoring model, which means you do not need to "pass" the individual sections, only the overall exam. Each section also has a specific weighting:
  - Domain 1: Cloud Concepts (26%)
  - Domain 2: Security and Compliance (25%)
  - Domain 3: Technology (33%)
  - Domain 4: Billing and Pricing (16%)

---

#### Domain 1: Cloud Concepts
1.1 Define the AWS Cloud and its value proposition<br>
1.2 Identify aspects of AWS Cloud economics<br>
1.3 List the different cloud architecture design principles


#### Domain 2: Security and Compliance
2.1 Define the AWS shared responsiblity model<br>
2.2 Define AWS Cloud security and compliance concepts<br>
2.3 Identify AWS Access management capabilities<br>
2.4 Identify resources for security support

#### Domain 3: Technology
3.1 Define methods of deploying and operating in the AWS Cloud<br>
3.2 Define the AWS global infrastructure<br>
3.3 Identify the core AWS services<br>
3.4 Identify resources for technology support

#### Domain 4: Billing and Pricing
4.1 Compare and contrast the various pricing models for AWS<br>
4.2 Recognize the various account structures in relation to AWS billing and pricing<br>
4.3 Identiofy resources available for billing support



