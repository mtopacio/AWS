## Domain 4: Billing and Pricing

References:
- [How AWS Pricing Works: AWS Pricing Overview](https://docs.aws.amazon.com/whitepapers/latest/how-aws-pricing-works/how-aws-pricing-works.pdf#welcome)
- [Support Plans](https://aws.amazon.com/premiumsupport/plans/)
