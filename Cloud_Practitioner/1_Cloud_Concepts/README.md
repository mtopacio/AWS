# Domain 1: Cloud Concepts

1.1 Define the AWS Cloud and its value proposition<br>
1.2 Identify aspects of AWS Cloud economics<br>
1.3 List the different cloud architecture design principles

---

Cloud computing is the on-demand delivery of compute power, database, storage, applications, and other IT resources through a cloud services platform via the internet with pay-as-you go pricing.

Benefits:

- _Trade capital expense for variable expense_ - pay only for what you use without the startup/maintenance capital for hardware
- _Benefit from massive economies of scale_ - splitting up processing amongst customers allows AWS to achieve "economies of scale", which gives you lower costs (i.e. multiple people per computer, Amazon pays for 1 computer, cost is split between customers)
- _Stop guessing capacity_ - use what you need when you need it. No more idle hardware.
- _Increase speed and agility_ - new resources in minutes
- _Stop spending money running/maintaining data centers_ - focus can be core business goals and not supportive functions
- _Go global in minutes_ - available in multiple regions around the world\

Cloud Computing Models:

- Infrastructure as a Service (IaaS)
- Platform as a Service (PaaS)
- Software as a Service (Saas)

AWS are steadily expanding infrastructure to help customers achieve lower latency. The AWS Cloud infrastructure is build around **Regions** and **Availability Zones**. An AWS Region is a physical location in the world where there are multiple Availability Zones. Availability Zones consist of one or more discrete data centers, each with redundant power, networking, and connectivity, housed in separate facilities (i.e. highly available, fault tolerant, and scalable). AWS has 80 availability zones within 25 geographic regions. 

The rest of the whitepaper reviews each of the services available from AWS.

References: 

- [Overview of Amazon Web Services](https://d1.awsstatic.com/whitepapers/aws-overview.pdf)
- [The pillars of the AWS Well-Architected Framework](https://docs.aws.amazon.com/wellarchitected/latest/framework/wellarchitected-framework.pdf#welcome)
