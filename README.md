# AWS Certifcations

This repo contains all the materials collected while trying to earn Amazon's certifications. At the time of writing, Amazon currently has the following 11 certifications:

![certifications.png](https://gitlab.com/mtopacio/AWS/-/raw/master/etc/images/certifications.png)

I'm trying for a career change into data science, so I'll be trying for the route outlined by _A Cloud Guru_'s [certification guide](https://gitlab.com/mtopacio/AWS/-/blob/master/etc/documents/Cert-Guide-AWS-2020.pdf).

![data_cert.png](https://gitlab.com/mtopacio/AWS/-/raw/master/etc/images/data_cert.png)



| Certification                    | Status  | Score |
| -------------------------------- | ------- | ----- |
| Cloud Practitioner               | WIP     |       |
| Solutions Architect Associate    | Pending |       |
| Developer Associate              | Pending |       |
| Database Specialty               | Pending |       |
| Data Analytics Specialty         | Pending |       |
| Machine Learning Specialty       | Pending |       |
| Solutions Architect Professional | Pending |       |

